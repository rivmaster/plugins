(function($) {
  $.fn.screen = function(data) {

    var delay = data.delay;
    var timer;
    var obj = this;

    obj.hide();
    this.html('<span class="screen-text">' + data.text + '</span>');
    var screenText = $('.screen-text');

    function flash() {

      var options = {
        duration: 3000,
        easing: 'linear'
      };

      screenText.animate({
        opacity: 0.1,
      }, options).animate({
        opacity: 0.8,
      }, options.duration, options.easing, function() {
        flash();
      });
    };

    function show() {
      timer = setTimeout(function() {
        obj.show();

        if (data.animation == true) {
          flash();
        }

      }.bind(this), delay);
    }

    show();

    $(document).keydown(function() {
      obj.hide();

      screenText.stop();

      clearTimeout(timer);
      show();
    });
  };

})(jQuery);


$(function() {
  var settings = {
    text: 'IMAQLIQ',
    delay: 5000,
    animation: true
  };
  $('#screen').screen(settings);
});
